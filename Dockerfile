FROM ubuntu:latest

RUN apt upgrade
RUN apt update
RUN apt -y install build-essential cmake 
RUN apt -y install libopenblas-dev liblapack-dev 
RUN apt -y install libx11-dev libgtk-3-dev

RUN apt -y install python3-pip
RUN pip3 install numpy
RUN pip3 install face_recognition

CMD face_recognition /input /output